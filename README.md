# README #

This is a sample for oauth2 and openid which can handle a simple login flow using both.

It only get's a basaic user info - name or email and displays it after log in.

It can be run with the command `mvn exec:java`

It will start an embedded tomcat server on port 8080, for the dex login to work it requires a dex server with a on http://192.168.60.12:5443



### What is this repository for? ###

* An adaptable demo for adding oauth to the channel server.

Currently supports facebook app
* * Facebook
* * Github
* * Dex OpenId

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact