/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example;

import com.example.tresting.util.DumbX509TrustManager;
import com.example.tresting.util.SSLContextRequestFactory;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import org.springframework.boot.context.embedded.EmbeddedServletContainerCustomizer;
import org.springframework.boot.web.servlet.ErrorPage;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpStatus;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.security.oauth2.client.OAuth2ClientContext;
import org.springframework.security.oauth2.client.OAuth2RestTemplate;
import org.springframework.security.oauth2.client.resource.OAuth2ProtectedResourceDetails;

@Configuration
public class ServletCustomizer {

    @Bean
    public EmbeddedServletContainerCustomizer customizer()
    {
        return container -> {
            container.addErrorPages(new ErrorPage(HttpStatus.UNAUTHORIZED, "/unauthenticated"));
        };
    }

    @Bean
    public OAuth2RestTemplate oauth2RestTemplate(OAuth2ProtectedResourceDetails resource, OAuth2ClientContext context)
    {
        OAuth2RestTemplate template = new OAuth2RestTemplate(resource, context);
        try {

            disableCertificateChecks(template);

        } catch (Exception ex) {
            Logger.getLogger(ServletCustomizer.class.getName()).log(Level.SEVERE, null, ex);
        }
        return template;
    }

    private static void disableCertificateChecks(OAuth2RestTemplate oauthTemplate) throws Exception
    {

        SSLContext sslContext = SSLContext.getInstance("TLS");
        sslContext.init(null, new TrustManager[]{new DumbX509TrustManager()}, null);
        ClientHttpRequestFactory requestFactory = new SSLContextRequestFactory(sslContext);
        //This is for OAuth protected resources
        oauthTemplate.setRequestFactory(requestFactory);
    }
}
