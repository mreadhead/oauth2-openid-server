/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.tresting.util;

import java.util.Arrays;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.oauth2.client.OAuth2ClientContext;
import org.springframework.security.oauth2.client.OAuth2RestTemplate;
import org.springframework.security.oauth2.client.resource.OAuth2ProtectedResourceDetails;
import org.springframework.security.oauth2.client.token.grant.code.AuthorizationCodeResourceDetails;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableOAuth2Client;

/**
 *
 * @author mike
 */
@Configuration
@EnableOAuth2Client
public class OpenIdConnectConfig {

    @Value("${dex.client.clientId}")
    private String clientId;

    @Value("${dex.client.clientSecret}")
    private String clientSecret;

    @Value("${dex.client.accessTokenUri}")
    private String accessTokenUri;

    @Value("${dex.client.userAuthorizationUri}")
    private String userAuthorizationUri;

    @Value("${dex.client.redirectUri}")
    private String redirectUri;

    @Value("${dex.client.issuer}")
    private String issuer;

    @Value("${dex.client.jwkUrl}")
    private String jwkUrl;

    @Bean
    public OAuth2ProtectedResourceDetails googleOpenId()
    {
        AuthorizationCodeResourceDetails details = new AuthorizationCodeResourceDetails();
        details.setClientId(clientId);
        details.setClientSecret(clientSecret);
        details.setAccessTokenUri(accessTokenUri);
        details.setUserAuthorizationUri(userAuthorizationUri);
        details.setScope(Arrays.asList("openid", "email"));
        details.setPreEstablishedRedirectUri(redirectUri);
        details.setUseCurrentUri(false);
        return details;
    }

    @Bean
    @Qualifier("googleOpenIdTemplate")
    public OAuth2RestTemplate googleOpenIdTemplate(OAuth2ClientContext clientContext)
    {
        return new OAuth2RestTemplate(googleOpenId(), clientContext);
    }

    public String getClientId()
    {
        return clientId;
    }

    public String getClientSecret()
    {
        return clientSecret;
    }

    public String getAccessTokenUri()
    {
        return accessTokenUri;
    }

    public String getUserAuthorizationUri()
    {
        return userAuthorizationUri;
    }

    public String getRedirectUri()
    {
        return redirectUri;
    }

    public String getIssuer()
    {
        return issuer;
    }

    public String getJwkUrl()
    {
        return jwkUrl;
    }

}
